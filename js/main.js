const vue = new Vue({
    el: 'main', //Es el ámbito donde va a poder acceder
    data: {
        mensaje: 'Hola mundos!',
        funcionaElShow: 'Funciona el Show',
        funcionaElIF: 'Funciona el IF con template',
        ok: true,
        contador: 0
    },
    methods: {
        contar: function () {
            this.contador += 1;
        }
    }
});